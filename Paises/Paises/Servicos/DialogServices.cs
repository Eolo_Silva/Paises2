﻿namespace Paises.Servicos
{
    using System.Windows.Forms;

    public class DialogServices
    {
        public void ShorMessage(string title, string message)
        {
            MessageBox.Show(message, title);
        }
    }
}
