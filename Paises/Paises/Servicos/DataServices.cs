namespace Paises.Servicos
{
    using Modelo;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Net;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public class DataServices
    {
        private SQLiteConnection connection;
        private SQLiteCommand command;
        private DialogServices dialogServices;

        public DataServices()
        {
            dialogServices = new DialogServices();

            if (!Directory.Exists(@"Data"))
            {
                Directory.CreateDirectory(@"Data");
            }

            var path = @"Data\PaisesDB.sqlite";
            connection = new SQLiteConnection("Data Source=" + path);
            connection.Open();
            try
            {
                //cria a tabela Paises
                string sqlcommand = "create table if not exists Paises (IDPais INTEGER PRIMARY KEY AUTOINCREMENT, name Varchar(250), capital Varchar(250), region Varchar(250), population int, flag Varchar(250))";
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                //cria a tabela Moedas
                sqlcommand = "create table if not exists Moedas (IDMoeda INTEGER PRIMARY KEY AUTOINCREMENT, code Varchar(50), nameMoeda Varchar(250), symbol Varchar(10) NULL, IDPais INTEGER, FOREIGN KEY(IDPais) REFERENCES Paises(IDPais))";
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                //cria tabela dominio
                sqlcommand = "create table if not exists Dominio (IDdominio INTEGER PRIMARY KEY AUTOINCREMENT, topLevelDomain Varchar(250), IDPais INTEGER, FOREIGN KEY(IDPais) REFERENCES Paises(IDPais))";
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                //cria a tabela diferencias horarias
                sqlcommand = "create table if not exists TimeZone (IDTimeZone INTEGER PRIMARY KEY AUTOINCREMENT, timezone Varchar(20), IDPais INTEGER, FOREIGN KEY(IDPais) REFERENCES Paises(IDPais))";
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();

                //cria tabela de atualizaçoes da BD
                sqlcommand = "create table if not exists versionsBD (IDVersion INTEGER PRIMARY KEY AUTOINCREMENT, versao Varchar(250))";
                command = new SQLiteCommand(sqlcommand, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception e)
            {
                dialogServices.ShorMessage("Erro", e.Message);
            }
        }
        public void SaveData(List<Pais> Paises)
        {
            //try
            //{
            string sql = null;
            foreach (var SalvarPais in Paises)
            {

                sql = string.Format("insert into Paises (name, capital, region, population, flag ) values (@name, @capital, @region, @population, @flag); SELECT last_insert_rowid()");
                //string path = Application.StartupPath + @"\\image\\" + SalvarPais.name;

                command = new SQLiteCommand(sql, connection);
                command.Parameters.AddWithValue("@name", SalvarPais.name);
                command.Parameters.AddWithValue("@capital", SalvarPais.capital);
                command.Parameters.AddWithValue("@region", SalvarPais.region);
                command.Parameters.AddWithValue("@population", SalvarPais.population);
                command.Parameters.AddWithValue("@flag", SalvarPais.flag);
                //command.Parameters.AddWithValue("@flag", path + ".jpg");
                int idPais = Convert.ToInt32(command.ExecuteScalar());
              


                //grava as moedas
                foreach (var moedas in SalvarPais.currencies)
                {
                    sql = string.Format("insert into Moedas (IDPais, code, nameMoeda, symbol) values (@IDPais, @code, @nameMoeda, @symbol)");

                    command = new SQLiteCommand(sql, connection);
                    command.Parameters.AddWithValue("@IDPais", idPais);
                    if(moedas.code !=null)
                        command.Parameters.AddWithValue("@code", moedas.code);
                    else
                        command.Parameters.AddWithValue("@code", "N/A");
                    if(moedas.name != null)
                    command.Parameters.AddWithValue("@nameMoeda", moedas.name);
                    else
                        command.Parameters.AddWithValue("@nameMoeda", "N/A");
                    if (moedas.symbol != null)
                        command.Parameters.AddWithValue("@symbol", moedas.symbol);
                    else
                        command.Parameters.AddWithValue("@symbol", "N/A");

                    command.ExecuteNonQuery();

                }
                foreach (var dominio in SalvarPais.topLevelDomain)
                {
                    sql = string.Format("insert into Dominio (IDPais, topLevelDomain) values (@IDPais, @topLevelDomain)");

                    command = new SQLiteCommand(sql, connection);
                    command.Parameters.AddWithValue("@IDPais", idPais);
                    command.Parameters.AddWithValue("@topLevelDomain", dominio.ToString());
                    command.ExecuteNonQuery();
                }
                //grava diferencias horarias dos paises
                foreach (var time in SalvarPais.timezones)
                {
                    sql = string.Format("insert into TimeZone (IDPais, timezone) values (@IDPais, @timezone)");

                    command = new SQLiteCommand(sql, connection);
                    command.Parameters.AddWithValue("@IDPais", idPais);
                    command.Parameters.AddWithValue("@timezone", time.ToString());
                    command.ExecuteNonQuery();
                }


            }
            //grava versão da actualização da BD
            sql = string.Format("insert into versionsBD (versao) values (@versao)");
            command = new SQLiteCommand(sql, connection);
            command.Parameters.AddWithValue("@versao", DateTime.Now.ToString());
            command.ExecuteNonQuery();
            connection.Close();
            //}
            //catch (Exception e)
            //{
            //    dialogServices.ShorMessage("Erro", e.Message);
            //}
        }
        public List<Pais> GetDataPais()
        {
            List<Pais> paises = new List<Pais>();
            try
            {

                string sql = "select IDPais, name, capital, region, population, flag from Paises";
                command = new SQLiteCommand(sql, connection);
                connection.Open();
                //lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    paises.Add(new Pais
                    {
                        IDPais = Convert.ToInt32(reader["IDPais"]),
                        name = (string)reader["name"],
                        capital = (string)reader["capital"],
                        region = (string)reader["region"],
                        population = (int)reader["population"],
                        flag = (string)reader["flag"],
                        currencies = GetDataMoedas(Convert.ToInt32(reader["IDPais"])),
                        topLevelDomain = GetDataDominios(Convert.ToInt32(reader["IDPais"])),
                        timezones = GetDataTimeZone(Convert.ToInt32(reader["IDPais"]))
                    });
                }
                return paises;
            }
            catch (Exception e)
            {
                dialogServices.ShorMessage("Erro get data pais", e.Message);
                return null;
            }
        }

        public List<Currency> GetDataMoedas(int cont)
        {
            List<Currency> moedas = new List<Currency>();
            try
            {
                string sql = "select code, nameMoeda, symbol, IDPais from Moedas where IDPais=" + cont + ";";
                command = new SQLiteCommand(sql, connection);
                //lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    moedas.Add(new Currency
                    {
                        code = (string)reader["code"],
                        name = (string)reader["nameMoeda"],
                        symbol = (string)reader["symbol"]
                    });

                }
                return moedas;
            }
            catch (Exception e)
            {
                dialogServices.ShorMessage("Erro get data moeda", e.Message);
                return null;
            }
        }
        public List<string> GetDataDominios(int cont)
        {
            List<string> dominios = new List<string>();
            try
            {
                string sql = "select topLevelDomain, IDPais from Dominio where IDPais=" + cont + ";";
                command = new SQLiteCommand(sql, connection);
                //lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    dominios.Add(reader["topLevelDomain"].ToString());
                }
                return dominios;
            }
            catch (Exception e)
            {
                dialogServices.ShorMessage("Erro get data dominio", e.Message);
                return null;
            }
        }
        public List<string> GetDataTimeZone(int cont)
        {
            List<string> timezone = new List<string>();
            try
            {
                string sql = "select IDPais, timezone from TimeZone where IDPais=" + cont + ";";
                command = new SQLiteCommand(sql, connection);
                //lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    timezone.Add(reader["timezone"].ToString());
                }
                
                return timezone;

            }
            catch (Exception e)
            {
                dialogServices.ShorMessage("Erro get data timezones", e.Message);
                return null;
            }
        }
        public string GetDataVersao()
        {
            string versao = "";
            try
            {
                string sql = "select versao from versionsBD;";
                command = new SQLiteCommand(sql, connection);
                //lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    versao = reader["versao"].ToString();
                }

                return versao;

            }
            catch (Exception e)
            {
                dialogServices.ShorMessage("Erro get data Versao", e.Message);
                return null;
            }
        }
        public void DeleteData()
        {
            try
            {
                connection.Open();
                string sql = "delete from Paises; update sqlite_sequence set seq=0 where name='Paises'";
                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();    
                sql = "delete from Moedas; update sqlite_sequence set seq=0 where name='Moedas'";
                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();
                sql = "delete from Dominio; update sqlite_sequence set seq=0 where name='Dominio'";
                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();
                sql = "delete from TimeZone; update sqlite_sequence set seq=0 where name='TimeZone'";
                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();
                
            }
            catch (Exception e)
            {
                dialogServices.ShorMessage("Erro delete data", e.Message);
            }
        }
    }
}
