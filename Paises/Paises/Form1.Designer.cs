﻿namespace Paises
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxOrigem = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.LabelResultado = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.creditosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atualizarAPIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RichTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelHoraPais = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelHoraLocal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ComboBoxOrigem
            // 
            this.ComboBoxOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxOrigem.FormattingEnabled = true;
            this.ComboBoxOrigem.Location = new System.Drawing.Point(421, 3);
            this.ComboBoxOrigem.Name = "ComboBoxOrigem";
            this.ComboBoxOrigem.Size = new System.Drawing.Size(313, 21);
            this.ComboBoxOrigem.TabIndex = 0;
            this.ComboBoxOrigem.SelectedIndexChanged += new System.EventHandler(this.ComboBoxOrigem_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(183, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Escolha um País para ver INFO :";
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox1.Location = new System.Drawing.Point(9, 227);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(168, 130);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 15;
            this.PictureBox1.TabStop = false;
            // 
            // LabelResultado
            // 
            this.LabelResultado.AutoSize = true;
            this.LabelResultado.Location = new System.Drawing.Point(560, 307);
            this.LabelResultado.Name = "LabelResultado";
            this.LabelResultado.Size = new System.Drawing.Size(43, 13);
            this.LabelResultado.TabIndex = 4;
            this.LabelResultado.Text = "Status";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(231, 343);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(0, 13);
            this.labelStatus.TabIndex = 5;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(525, 281);
            this.ProgressBar1.Maximum = 300;
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(179, 14);
            this.ProgressBar1.TabIndex = 6;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.creditosToolStripMenuItem,
            this.atualizarAPIToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(747, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // creditosToolStripMenuItem
            // 
            this.creditosToolStripMenuItem.Name = "creditosToolStripMenuItem";
            this.creditosToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.creditosToolStripMenuItem.Text = "Creditos";
            this.creditosToolStripMenuItem.Click += new System.EventHandler(this.CreditosToolStripMenuItem_Click);
            // 
            // atualizarAPIToolStripMenuItem
            // 
            this.atualizarAPIToolStripMenuItem.Name = "atualizarAPIToolStripMenuItem";
            this.atualizarAPIToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.atualizarAPIToolStripMenuItem.Text = "Atualizar API";
            this.atualizarAPIToolStripMenuItem.Click += new System.EventHandler(this.atualizarAPIToolStripMenuItem_Click);
            // 
            // RichTextBox1
            // 
            this.RichTextBox1.Location = new System.Drawing.Point(488, 64);
            this.RichTextBox1.Name = "RichTextBox1";
            this.RichTextBox1.Size = new System.Drawing.Size(246, 201);
            this.RichTextBox1.TabIndex = 10;
            this.RichTextBox1.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(83, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Hora do País : ";
            // 
            // LabelHoraPais
            // 
            this.LabelHoraPais.BackColor = System.Drawing.Color.Transparent;
            this.LabelHoraPais.Location = new System.Drawing.Point(183, 64);
            this.LabelHoraPais.Name = "LabelHoraPais";
            this.LabelHoraPais.Size = new System.Drawing.Size(299, 199);
            this.LabelHoraPais.TabIndex = 12;
            this.LabelHoraPais.Text = "00:00:00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(96, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Hora Local : ";
            // 
            // LabelHoraLocal
            // 
            this.LabelHoraLocal.BackColor = System.Drawing.Color.Transparent;
            this.LabelHoraLocal.Location = new System.Drawing.Point(183, 38);
            this.LabelHoraLocal.Name = "LabelHoraLocal";
            this.LabelHoraLocal.Size = new System.Drawing.Size(152, 21);
            this.LabelHoraLocal.TabIndex = 14;
            this.LabelHoraLocal.Text = "00:00:00";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Paises.Properties.Resources.paises;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(747, 367);
            this.Controls.Add(this.LabelHoraLocal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LabelHoraPais);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RichTextBox1);
            this.Controls.Add(this.ProgressBar1);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.LabelResultado);
            this.Controls.Add(this.PictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxOrigem);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxOrigem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox PictureBox1;
        private System.Windows.Forms.Label LabelResultado;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.ProgressBar ProgressBar1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem creditosToolStripMenuItem;
        private System.Windows.Forms.RichTextBox RichTextBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelHoraPais;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelHoraLocal;
        private System.Windows.Forms.ToolStripMenuItem atualizarAPIToolStripMenuItem;
    }
}

