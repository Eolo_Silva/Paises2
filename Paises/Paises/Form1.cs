﻿namespace Paises
{
    using Modelo;
    using Servicos;
    using Svg;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Net;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.IO;
    using Properties;

    public partial class Form1 : Form
    {
        #region atributo
        private List<Pais> Paises;
        private NetworkServices networkService;
        private ApiServices apiService;
        private DialogServices dialogService;
        private DataServices dataService;
        #endregion


        bool Verificador = false;
        public Form1()
        {
            InitializeComponent();
            networkService = new NetworkServices();
            apiService = new ApiServices();
            dialogService = new DialogServices();
            dataService = new DataServices();
            LoadFlags();
        }
        private async void LoadFlags()
        {
            bool Load;
            ProgressBar1.Value = 0;

            LabelResultado.Text = "A Atualizar Base de Dados Local...";

            var connection = networkService.CheckConnection();
            //se nao houver ligação a internet usa base de dados local
            if (!connection.IsSuccess)
            {
                LoadLocalFlags();
                Load = false;
            }
            //com ligação a internet
            else

            {
                await LoadApiFlags();

                Load = true;
            }

            //se ambas ligações fracassarem mostra mensagem do porquê
            if (Paises.Count == 0)
            {
                LabelResultado.Text = "Não Foram Carregados os Dados..";

                labelStatus.Text = "Primeira inicialização deverá ter ligãção a internet";
                return;
            }
            ProgressBar1.Value = 20;
            LabelResultado.Text = "Convertendo as Bandeiras Aguarde!...";

            //metodo que descarrega todas as bandeiras e converte em jpg, depois utilizar sem ligação a internet
            CarregarBandeiras();

            ComboBoxOrigem.DataSource = Paises;
            ComboBoxOrigem.DisplayMember = "name";
            Verificador = true;
            ComboBoxOrigem.SelectedIndex = -1;



            if (Load)
            {
                labelStatus.Text = string.Format("Paises carregados da internet em {0:F}", DateTime.Now);
            }
            else
            {
                labelStatus.Text = string.Format("Ultima Atualização em : {0}", dataService.GetDataVersao());
            }

        }

        private void CarregarBandeiras()
        {
            try
            {

                if (!Directory.Exists(@"..\..\image"))
                {
                    Directory.CreateDirectory(@"..\..\image");

                    var n = 20;
                    foreach (var item in Paises)
                    {
                        if (item.flag != "https://restcountries.eu/data/iot.svg")
                        {
                            ProgressBar1.Value = n;

                            string path = @"..\\..\\image\\" + item.name;

                            if (!File.Exists(path + ".jpg"))
                            {
                                using (WebClient webClient = new WebClient())
                                {
                                    webClient.DownloadFile(item.flag, path);
                                    ProgressBar1.Value = n++;
                                }

                                var svgDocument = SvgDocument.Open(path);
                                var bitmap = svgDocument.Draw();
                                bitmap.Save(path + ".jpg", ImageFormat.Jpeg);
                            }
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            LabelResultado.Text = "Processo Terminado!, Pode Começar...";
            ProgressBar1.Value = 300;

            RichTextBox1.Clear();
        }



        private void LoadLocalFlags()
        {
            Paises = dataService.GetDataPais();

            ProgressBar1.Value = 300;

        }

        private async Task LoadApiFlags()
        {

            ProgressBar1.Value = 20;

            var response = await apiService.GetFlags("https://restcountries.eu", "/rest/v2/all");

            Paises = (List<Pais>)response.Result;

            dataService.DeleteData();

            dataService.SaveData(Paises);
        }

        private void CreditosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Autor: Eolo Guerra Silva Versão: 1.0 Data: 07/11/2017");
        }

        private void ComboBoxOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {

                ProgressBar1.Visible = false;
                LabelResultado.Visible = false;
                if (Verificador == false) return;

                LabelHoraPais.Text = "";

                RichTextBox1.Clear();
                if (ComboBoxOrigem.SelectedIndex > -1)
                {
                    try
                    {
                        Pais selecao = new Pais();
                        selecao = (Pais)ComboBoxOrigem.SelectedItem;
                        var currency = selecao.currencies;
                        var domain = selecao.topLevelDomain;
                        //informação do Pais
                        RichTextBox1.Text = "Nome do País: " + selecao.name + "\nCapital: " + selecao.capital + "\nRegião: " + selecao.region + "\nPopulação: " + selecao.population + "\n";
                        //informação da moeda do pais selecionado
                        foreach (var moeda in selecao.currencies)
                        {
                            RichTextBox1.Text += "\nCódigo da Moeda: " + moeda.code + "\nNome da Moeda: " + moeda.name + "\nSímbolo da Moeda: " + moeda.symbol;
                        }

                        //informação do dominio na internet do pais selecionado
                        foreach (var nomenclatura in selecao.topLevelDomain)
                        {
                            RichTextBox1.Text += "\nDomínio da Internet: " + nomenclatura.ToString();
                        }

                        PictureBox1.Image = Image.FromFile(@"..\..\image\" + selecao.name + ".jpg");

                        foreach (var hora in selecao.timezones)
                        {
                            if (hora != "UTC")
                            {
                                if (hora.Length == 6)
                                {
                                    LabelHoraLocal.Text = string.Format("{0}", DateTime.Now);
                                    TimeSpan duration = new TimeSpan(
                                            0, Convert.ToInt32(hora.Substring(3, 3)), 0, 0);
                                    DateTime horapais = DateTime.Now.Add(duration);
                                    LabelHoraPais.Text = LabelHoraPais.Text + horapais.ToString() + " (" + hora.ToString() + ":00)" + Environment.NewLine;

                                }
                                else
                                {
                                    LabelHoraLocal.Text = string.Format("{0}", DateTime.Now);
                                    TimeSpan duration = new TimeSpan(
                                            0, Convert.ToInt32(hora.Substring(3, 3)), Convert.ToInt32(hora.Substring(7, 2)), 0);
                                    DateTime horapais = DateTime.Now.Add(duration);
                                    LabelHoraPais.Text = LabelHoraPais.Text + horapais.ToString() + " (" + hora.ToString() + ")" + Environment.NewLine;
                                }

                            }
                            else
                            {
                                LabelHoraLocal.Text = string.Format("{0}", DateTime.Now);
                                LabelHoraPais.Text = DateTime.Now.ToString() + Environment.NewLine;
                            }
                        }

                    }

                    catch
                    {
                        PictureBox1.Image = Resources.FlagErro;
                    }
                
            }
        }

        private void atualizarAPIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFlags();
        }
    }
}

