﻿namespace Paises.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Currency
    {
        public int IDPais { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string symbol { get; set; }
    }
}
