﻿namespace Paises.Modelo
{
    using System.Collections.Generic;
    public class Pais
    {
        //propriedades que são usadas no projeto, 
        //foram filtradas da classe que recebe todas as propriedades da API
        public int IDPais { get; set; }
        public string name { get; set; } 
        public string region { get; set; }
        public int population { get; set; }
        public string capital { get; set; }
        public string flag { get; set; }
        public List<Currency> currencies { get; set; }
        public List<string> topLevelDomain { get; set; }
        public List<string> timezones { get; set; }
    }
}
